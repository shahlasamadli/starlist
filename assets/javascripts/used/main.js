$(document).ready(function () {

    ///// input see password

    $('.showPassword').click(function () {
        let inputType = $(this).siblings('input').attr('type');

        if (inputType === 'password') {
            $(this).siblings('input').attr('type', 'text');
            $(this).removeClass('sprite--eye').addClass('sprite--eye-hide');
        } else {
            $(this).siblings('input').attr('type', 'password');
            $(this).removeClass('sprite--eye-hide').addClass('sprite--eye');

        }
    });

    ////group input

    $('.group__input').focusin(function () {
        $(this).siblings('label').addClass('active');
        $(document).on('focusin', '.group__input', function () {
            $(this).siblings('label').addClass('active')
            $(this).addClass('active');
        });

    });

});
