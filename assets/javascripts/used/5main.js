$(document).ready(function () {

    // fedeOut
    $(function () {
        $(".btn-login").click(function () {
            $(".login_popup").fadeIn(), "easeOutExpo";
            $(".overlay").fadeIn(), "easeOutExpo";
            return false;
        });
        $(".overlay").click(function () {
            $(".login_popup").fadeOut(), "easeOutExpo";
            $(".login_table").delay(400).animate(
                {
                    left: 0
                },
                600,
                "easeOutExpo"
            );
            $(".forget_table").delay(400).animate(
                {
                    left: 400
                },
                600,
                "easeOutExpo"
            );
            return false;
        });
        $(".glyphicon-remove").click(function () {
            $(".login_popup").fadeOut(), "easeOutExpo";
            $(".login_table").delay(400).animate(
                {
                    left: 0
                },
                600,
                "easeOutExpo"
            );
            $(".forget_table").delay(400).animate(
                {
                    left: 400
                },
                600,
                "easeOutExpo"
            );
            return false;
        });
    });


//
    $(function () {
        $(".forget").click(function () {
            $(".login_table").animate(
                {
                    left: -500
                },
                600,
                "easeOutExpo"
            );
            $(".forget_table").animate(
                {
                    left: 0
                },
                600,
                "easeOutExpo"
            );
            return false;
        });
        $(".back_login").click(function () {
            $(".login_table").animate(
                {
                    left: 0
                },
                600,
                "easeOutExpo"
            );
            $(".forget_table").animate(
                {
                    left: 400
                },
                600,
                "easeOutExpo"
            );
            return false;
        });
    });


// Product popup

    $(".product__button").click(function () {
        // $(".product_popup").fadeIn(), "easeOutExpo";
        $(".product_popup").addClass('show');
        $('body').addClass('noscroll')
        $(".overlay").fadeIn(), "easeOutExpo";
        return false;

    });
    $(".overlay").click(function () {

        $(".product_popup").removeClass('show');
        $('body').removeClass('noscroll')

        // $(".product_popup").fadeOut(), "easeOutExpo";
        // $(".login_table").delay(400).animate(
        //     {
        //         left: 0
        //     },
        //     600,
        //     "easeOutExpo"
        // );
    });
// Product popup end

    if ($('.slider-single').length) {

        $('.slider-single').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            adaptiveHeight: true,
            infinite: false,
            useTransform: true,
            speed: 400,
            asNavFor: '.slider-nav'
            //cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
        });


        $('.slider-nav')
            .on('init', function (event, slick) {
                $('.slider-nav .slick-slide.slick-current').addClass('is-active');
            })
            .slick({

                slidesToShow: 8,
                slidesToScroll: 8,
                dots: false,
                focusOnSelect: false,
                infinite: false,
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5,
                    }
                }, {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4
                    }
                }, {
                    breakpoint: 420,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                }]
            });

        $('.slider-single').on('afterChange', function (event, slick, currentSlide) {
            $('.slider-nav').slick('slickGoTo', currentSlide);
            var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
            $('.slider-nav .slick-slide.is-active').removeClass('is-active');
            $(currrentNavSlideElem).addClass('is-active');
        });

        $('.slider-nav').on('click', '.slick-slide', function (event) {
            event.preventDefault();
            var goToSingleSlide = $(this).data('slick-index');

            $('.slider-single').slick('slickGoTo', goToSingleSlide);
        });
    }


    // if ($('.uploaded-filename').text('')) {
    //     $('div.name-holder').css('display', 'none');
    // }

/////////////////////////////  Image Upload


    function ImageSetter(input, target) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                target.attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(".imgInp").change(function () {
        var imgDiv = $(this).data('id');
        imgDiv = $('#' + imgDiv);
        ImageSetter(this, imgDiv);
    });

    $(document).on('click', '.file-input2', function () {
        //let a = $(this).parent().parent('.upload').find('input').attr('id');
        //let a = $(this).attr('id');
        $(this).parent().parent('.upload').find('input').trigger('click');

        //console.log(a);
    });
    /////////////////////////////  Image Upload end


    /////////////// add input


    $(document).on('click', '.addMaker', function (e) {
        e.preventDefault();
        let inputVal = $(this).siblings('.add_user_name').val();
        inputVal = inputVal.trim();

        if (inputVal.length > 0) {
            $('.addMakersBlock').append(' <li class="mb-3">\n' +
                '                                        <div class="post__makers-info">\n' +
                '                                           <div class="d-flex justify-content-between">\n' +
                '                                                   <div class="mr-2">\n' +
                '                                                <img src="assets/images/small-logo.png" alt="" class="post__makers-profile">\n' +
                '                                            </div>\n' +
                '                                            <div class="mr-5">\n' +
                '                                                <p class="post__makers-name">' + inputVal + '</p>\n' +
                '                                                <p class="post__makers-status">Has not yet joined Product Hunt</p>\n' +
                '                                            </div></div>\n' +
                '                                            <div >\n' +
                '                                            <span class="removeMakers">remove</span>\n' +
                '                                        </div>\n' +
                '\n' +
                '                                    </li>')
        }


        $('.add_user_name').val('');

    })

    $(document).on('click', '.removeMakers', function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    })


});

$('#hashtag').amsifySuggestags({
    type: 'bootstrap',
    //suggestions: ['Black', 'White', 'Red', 'Blue', 'Green', 'Orange'],
    tagLimit: 5
});
$('#hashtag-categories').amsifySuggestags({
    type: 'bootstrap',
    suggestions: ['Development', 'Design', 'Game', 'Management', 'Software Development', 'Hardware', 'AI', 'AR', 'VR', 'MR'],
    tagLimit: 5
});

/////////////


/*$('input[type="file"]').change(function (e) {

    var fileName = e.target.files[0].name;
    var path = document.getElementById("upload_imgs").value;

    alert(fileName + ' ' + path);

});*/


// Click button to activate hidden file input
/*
$('.fileuploader-btn').on('click', function(){
    $('.fileuploader').click();
});
*/

// Click above calls the open dialog box
// Once something is selected the change function will run
$('#upload_imgs[type="file"]').change(function (e) {


// Create new FileReader as a variable
    var reader = new FileReader();
    var fileName = e.target.files[0].name;

/*    var allowedVideoFiles = [".mp4", ".webm"];
    var allowedImgFiles = [".jpg", ".png", ".jpeg"];*/

    fileName = fileName.split('.');

    fileName2 = fileName[fileName.length - 1];


    if (fileName2 === 'mp4' || fileName2 === 'webm') {

        // Onload Function will run after video has loaded
        reader.onload = function (file) {
            var fileContent = file.target.result;
            $('#img_preview').append('<video src="' + fileContent + '" controls></video>');
        };

        // Get the selected video from Dialog
        reader.readAsDataURL(this.files[0]);
    }  else if(fileName2 === 'jpg' || fileName2 === 'png' || fileName2 === 'jpeg' ) {
        // Onload Function will run after video has loaded
        reader.onload = function (file) {
            var fileContent = file.target.result;
            $('#img_preview').append('<img src="' + fileContent + '" controls></img>');
        };

        // Get the selected video from Dialog
        reader.readAsDataURL(this.files[0]);
    }



});


///////////////////////


/*
var imgUpload = document.getElementById("upload_imgs"),
    imgPreview = document.getElementById("img_preview"),
    videoPreview = document.getElementById("img_preview"),
    imgUploadForm = document.getElementById("img-upload-form"),
    totalFiles,
    previewTitle,
    previewTitleText,
    img,
    video;

imgUpload.addEventListener("change", previewImgs, false);
imgUploadForm.addEventListener(
    "submit",
    function (e) {
        e.preventDefault();
        alert(
            "Images Uploaded! (not really, but it would if this was on your website)"
        );
    },
    false
);



function previewImgs(event) {

    totalFiles = imgUpload.files.length;

    // if (!!totalFiles) {
    //     imgPreview.classList.remove("quote-imgs-thumbs--hidden");
    //     previewTitle = document.createElement("p");
    //     previewTitle.style.fontWeight = "bold";
    //     previewTitleText = document.createTextNode(
    //         totalFiles + " Total Images Selected"
    //     );
    //     previewTitle.appendChild(previewTitleText);
    //     imgPreview.appendChild(previewTitle);
    // }

    for (var i = 0; i <= totalFiles; i++) {
        var allowedVideoFiles = [".mp4", ".webm"];
        var allowedImgFiles = [".jpg", ".png", ".jpeg"];


        if (allowedVideoFiles) {
            console.log(allowedVideoFiles);
            console.log('videodur')

            /!* video = document.createElement("video");
             video.src = URL.createObjectURL(event.target.files[i]);
             video.classList.add("img-preview-thumb");
             videoPreview.appendChild(video);*!/

        } else if (allowedImgFiles) {
            console.log('shekildir')

            /!*img = document.createElement("img");
            img.src = URL.createObjectURL(event.target.files[i]);
            img.classList.add("img-preview-thumb");
            imgPreview.appendChild(img);*!/
        }

    }
    // for (var j = 0; j< totalFiles; j++){
    //     video = document.createElement("video");
    //     video.src = URL.createObjectURL(event.target.files[j]);
    //     video.classList.add("img-preview-thumb");
    //     videoPreview.appendChild(video);
    // }
}
*/
